build:	
	echo "null build"

clean:	
	echo "null clean"

dist:	
	rm -f hamexam.orig.tar.gz
	tar zcvf hamexam.orig.tar.gz --exclude debian *

install:	
	mkdir -p $(DESTDIR)/usr/share
	cp -r share $(DESTDIR)/usr/share/hamexam
	mkdir -p $(DESTDIR)/usr/bin
	ln -s ../share/hamexam/hamexam $(DESTDIR)/usr/bin/hamexam
	mkdir -p $(DESTDIR)/usr/share/man/man1
	gzip -c hamexam.1 > $(DESTDIR)/usr/share/man/man1/hamexam.1.gz
	mkdir -p $(DESTDIR)/usr/share/applications
	cp hamexam.desktop $(DESTDIR)/usr/share/applications/hamexam.desktop
	mkdir -p $(DESTDIR)/usr/share/pixmaps
	cp hamexam.png $(DESTDIR)/usr/share/pixmaps/hamexam.png

