# split question pool file into separate file for each question
# Copyright (C) 2011-2019 John Nogatch AC6SL <jnogatch@gmail.com>

# recognize new question/answer info
/^[EGT][0-9][A-Z][0-9][0-9] \([A-D])/{
    question = $1

    # continue copying lines until does not start with ordinary text
    while ($1 ~ /[A-Za-z0-9 ]/) {
	# remove spurious \r
	{gsub( /\r/, "")}

	# replace \377 spaces
	{gsub( /\377/, " ")}

	# remove spurious \302
	{gsub( /\302/, "")}

	# replace \222 apostrophes
	{gsub( /\222/, "\47")}

	# replace \223 quotes
	{gsub( /\223/, "\42")}

	# replace \224 quotes
	{gsub( /\224/, "\42")}

	# replace \226 hyphen
	{gsub( /\226/, "-")}

	# replace \240 space
	{gsub( /\240/, " ")}

	# replace 3-char exponent leader
	{gsub( / \342\200.E/, "^")}

	# replace 3-char apostrophes
	{gsub( /\342\200\231/, "\47")}

	# replace 3-char quotes
	{gsub( /\342\200\234/, "\42")}

	# replace 3-char quotes
	{gsub( /\342\200\235/, "\42")}

	# this print for debug
	# print "\"" question "\"" $0

	# output modified line to question file
	print $0 > question

	# read another line & exit if error
	if (getline != 1) break
	}

    # avoid leaving too many descriptors open
    close( question)
    }

