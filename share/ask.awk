# display question, possible answers, then process reply
# Copyright (C) 2011-2019 John Nogatch <jnogatch@gmail.com>

# collect info
(NR==1){answer = $0; key = $2; gsub( /[^A-D]/, "", key); next}

# display question and possible answers
{print $0; next}

# read response from stdin, if wrong, print correct answer info and wait for ack
END {
    # read user's response
    if (getline < "/dev/stdin" != 1)
	exit 2

    # convert to upper case
    response = toupper( $1)

    # check for "quit"
    if (response == "Q")
	exit 2

    # check for incorrect response
    if (response != key) {
	print "*******" answer "\nto continue, hit Enter"
	if (getline < "/dev/stdin" != 1)
	    exit 2
	exit 1
	}

    # response was correct
    print ".......correct\n"
    exit 0
    }

